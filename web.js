var gzippo = require('gzippo');
var express = require('express');
var app = express();
var path = require('path');
 
  app.use(gzippo.staticGzip("" + __dirname + "/developer-v1.2"));
  // app.use(express.static(path.join(__dirname, 'node_modules')));
  app.use('/assets', express.static(__dirname + '/developer-v1.2/assets'));
  app.listen(process.env.PORT || 5000);
  console.log("listening on port: " + process.env.PORT);
  